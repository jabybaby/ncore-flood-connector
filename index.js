if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}
const ncore = require('./ncore-scraper.js');
const imdb = require('./imdb-scraper.js');

const Api = require('flood-api').default;

const api = new Api({
    baseUrl: process.env.FLOOD_URL,
    username: process.env.FLOOD_USERNAME,
    password: process.env.FLOOD_PASSWORD,
})

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

//use json and urlencoded middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const searchIMDB = async (query) => {
    let ncoreData = await ncore.searchIMDBOnly(query);
    let imdbData = await Promise.all(ncoreData.map(el => imdb(el.IMDBid)))

    imdbData.forEach((el) => {
        el.torrents = ncoreData.find(e => e.IMDBid == el.id).torrents
    })
    
    return imdbData.sort((a,b) => b.torrents.length - a.torrents.length);
}

app.set("view engine", "ejs");

app.get('/', (req, res) => {
    console.log("f");
    res.render('search');
});

app.get("/api/:query", (req, res) => {
    searchIMDB(req.params.query).then(data => {
        res.json(data);
    })
})
app.get("/api/raw/:query", (req, res) => {
    ncore.search(req.params.query).then(data => {
        res.json(data);
    })
})

app.post("/search", (req, res) => {
    res.redirect(`/search/${req.body.query}`);
})

app.get("/search/:query", (req, res) => {
    searchIMDB(req.params.query).then(data => {
        res.render('search', {results: data});
    })
})

app.get("/search/:query/:selectedMovie", (req, res) => {
    searchIMDB(req.params.query).then(data => {
        res.render('search', {results: data, selected: req.params.selectedMovie});
    })
})

app.get("/raw", (req, res) => {
    res.render('raw');
});
app.post("/raw", (req, res) => {
    res.redirect(`/raw/${req.body.query}`);
})

app.get("/raw/:query", (req, res) => {
    ncore.search(req.params.query).then(data => {
        res.render('raw', {results: data});
    })
})

app.post("/download", async (req, res) => {
    if(!req.body || !req.body.torrents || !req.body.torrents[0] || !req.body.torrents[0].type || !req.body.torrents[0].type.type3){
        return res.status(400).json({error: "Missing data"});
    }
    switch(req.body.torrents[0].type.type3){
        case "Movie":
            res.json(await api.torrents.addUrls({
                urls: [`https://ncore.pro/torrents.php?action=download&id=${req.body.torrents[0].id}&key=${process.env.NCORE_KEY}`],
                destination: process.env.FLOOD_PATH+(process.env.FLOOD_PATH.endsWith("/")?"":"/")+"Movies/"+req.body.title+" ("+req.body.year+")",
                tags: ["Movies", "AutoDownloader"],
                start: true
            }))
            break;
        case "Series":
            res.json(await api.torrents.addUrls({
                urls: [`https://ncore.pro/torrents.php?action=download&id=${req.body.torrents[0].id}&key=${process.env.NCORE_KEY}`],
                destination: process.env.FLOOD_PATH+(process.env.FLOOD_PATH.endsWith("/")?"":"/")+"Series/"+req.body.title,
                tags: ["Series", "AutoDownloader"],
                start: true
            }));
            break;
        default:
            res.json(await api.torrents.addUrls({
                urls: [`https://ncore.pro/torrents.php?action=download&id=${req.body.torrents[0].id}&key=${process.env.NCORE_KEY}`],
                destination: process.env.FLOOD_PATH+(process.env.FLOOD_PATH.endsWith("/")?"":"/")+req.body.torrents[0].type.type3,
                tags: [req.body.torrents[0].type.type3, "AutoDownloader"],
                start: true
            }))
            break;
    }
})

app.listen(port, () => {
    console.log(`PlexDash listening on port ${port}`);
});
